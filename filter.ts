function isPair(number: number): boolean {
    return number % 2 == 0;
}

function isGreaterThanThree(number: number): boolean {
    return number > 3;
}

function startWithCap(word: string): boolean {
    return word[0].toUpperCase() == word[0];
}


var inc:(x: number) => number = number => number++;


function filter<A>(xs:A[], f: (x: A) => boolean): A[] {
    var ys:A[] = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i])) {
            ys.push(xs[i]);
        }
    }
    return ys;
}

console.log(filter(["peter", "Ann", "Rose", "louise"], startWithCap));  // ["Ann", "Rose"]
console.log(["peter", "Ann", "Rose", "louise"].filter(startWithCap));  // ["Ann", "Rose"]