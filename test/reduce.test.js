import { reduce, add, max } from '../basic_functions';

test('reduce([1, 3, 4, 5, 6], add, 0) should be 19', () => {
    expect(reduce([1, 3, 4, 5, 6], add, 0)).toBe(19);
});

test('reduce([1, 3, 4, 5, 6], max, 0)) should be 6', () => {
    expect(reduce([], max, 0)).toBe(0);
});

test('reduce([], max, 0)) should be 6', () => {
    expect(reduce([], max, 0)).toBe(0);
});

test('reduce([], max)) should fail', () => {
    expect(() => reduce([], max)).toThrow(new Error("Empty array and no initial value"));
});

test('reduce([1, 3, 4, 5, 6], max) should be 6', () => {
    expect(reduce([1, 3, 4, 5, 6], add)).toBe(19);
});
