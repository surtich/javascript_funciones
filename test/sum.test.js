import { sum } from '../sum';

test('1 + 2 should be 3', function () {
  expect(sum(1, 2)).toBe(3);
});