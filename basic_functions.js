var numbers = [1, 6, 3, 2, 9];

function map(xs, f) {
    var ys = [];
    for (var i = 0; i < xs.length; i++) {
        ys.push(f(xs[i]));
    }
    return ys;
}

function isPair(number) {
    return number % 2 == 0;
}

var inc = number => number++;

function double(number) {
    return number * 2;
}

function capitalize(word) {
    return word[0].toUpperCase() + word.slice(1);
}

function incAll(numbers) {
    return map(numbers, inc);
}

console.log(map(["peter", "ann", "rose"], capitalize));
console.log(["peter", "ann", "rose"].map(capitalize));
console.log(map(numbers, isPair));
console.log(incAll(numbers));
console.log(numbers.map(inc));
console.log(map(numbers, number => number * 3));



function pairs(xs) {
    var ys = [];
    for (var i = 0; i < xs.length; i++) {
        if (isPair(xs[i])) {
            ys.push(xs[i])
        }
    }
    return ys;

}


function greatThanThree(xs) {
    var ys = [];
    for (var i = 0; i < xs.length; i++) {
        if (xs[i] > 3) {
            ys.push(xs[i])
        }
    }
    return ys;

}


function caps(xs) {
    var ys = [];
    for (var i = 0; i < xs.length; i++) {
        if (xs[i][0].toUpperCase() == xs[i][0]) {
            ys.push(xs[i])
        }
    }
    return ys;
}


function filter(xs, f) {
    var ys = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i])) {
            ys.push(xs[i]);
        }
    }
    return ys;
}



console.log(pairs([1, 3, 4, 5, 6])); // [4, 6]
console.log(greatThanThree([1, 3, 4, 5, 6])); // [4, 5, 6]
console.log(caps(["peter", "Ann", "Rose", "louise"]));  // ["Ann", "Rose"]

//console.log(max([1, 3, 4, 5, 6])); // 6
//console.log(acronymize(["peter", "Ann", "Rose", "louise"]));  // "PARL"

function sum(numbers) {
    var acc = 0;
    for (var i = 0; i < numbers.length; i++) {
        acc += numbers[i];
    }
    return acc;
}

export var add = (x, y) => x + y;

export var max = (x, y) => x > y ? x : y;

export function reduce(xs, f, init) {

    var acc = init;
    var i = 0;

    if (arguments.length == 2) {
        if (xs.length == 0) {
            throw new Error("Empty array and no initial value");
        } else {
            acc = xs[0];
            i = 1;
        }
    }

    for (; i < xs.length; i++) {
        acc = f(acc, xs[i]);
    }
    return acc;
}
