function isPair(number: number): boolean {
    return number % 2 == 0;
}

var inc:(x: number) => number = number => number+1;


function capitalize(word: string): string {
    return word[0].toUpperCase() + word.slice(1);
}

function map<A, B>(xs:A[], f: (x:A) => B): B[] {
    var ys: B[] = [];
    for (var i = 0; i < xs.length; i++) {
        ys.push(f(xs[i]));
    }
    return ys;
}

function recuMap<A, B>(xs: A[], f: (x:A) => B): B[] {
    if (xs.length == 0) {
        return [];
    } else {        
        return [f(xs[0]), ...recuMap(xs.slice(1), f)];
    }
}


console.log(map([1,2,3,4], isPair));
